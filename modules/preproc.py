from sklearn.linear_model import LinearRegression
import numpy as np
import pandas as pd

MAIN_BODY_POINTS = list(range(0, 23))
POSE_POINTS = list(range(0, 33))
HANDS_LINE_POINTS = [21, 15, 17, 19, 15, 13, 11, 12, 14, 16, 20, 18, 16, 22]
FACE_LINE_POINTS = [0, 10, 9, 0, 8, 6, 5, 4, 0, 7, 3, 2, 1, 0]
LEGS_LINE_POINTS = [28, 32, 30, 28, 26, 24, 23, 25, 27, 29, 31, 27]
BOXBODY_LINE_POINTS = [11, 12, 24, 23, 11]

POSE_ENUM = dict(NOSE=0,
                 LEFT_EYE_INNER=1,
                 LEFT_EYE=2,
                 LEFT_EYE_OUTER=3,
                 RIGHT_EYE_INNER=4,
                 RIGHT_EYE=5,
                 RIGHT_EYE_OUTER=6,
                 LEFT_EAR=7,
                 RIGHT_EAR=8,
                 MOUTH_LEFT=9,
                 MOUTH_RIGHT=10,
                 LEFT_SHOULDER=11,
                 RIGHT_SHOULDER=12,
                 LEFT_ELBOW=13,
                 RIGHT_ELBOW=14,
                 LEFT_WRIST=15,
                 RIGHT_WRIST=16,
                 LEFT_PINKY=17,
                 RIGHT_PINKY=18,
                 LEFT_INDEX=19,
                 RIGHT_INDEX=20,
                 LEFT_THUMB=21,
                 RIGHT_THUMB=22,
                 LEFT_HIP=23,
                 RIGHT_HIP=24,
                 LEFT_KNEE=25,
                 RIGHT_KNEE=26,
                 LEFT_ANKLE=27,
                 RIGHT_ANKLE=28,
                 LEFT_HEEL=29,
                 RIGHT_HEEL=30,
                 LEFT_FOOT_INDEX=31,
                 RIGHT_FOOT_INDEX=32,
                 )

# Functions:
# 2) preprocess csv - points 

""" 
Points coordinates: xs, yx, zs
"""


def get_pose_points_coordinates(df_pose, points, i_row='all', to_np=True, coord_type='local'):
    coord_types = ['local', 'global']
    assert coord_type in coord_types, f"Error! Invalide coord_type={coord_type}. Possible values: {coord_types}"

    if i_row == 'all':
        df_pose = df_pose.iloc[:, :]
    else:
        df_pose = df_pose.iloc[i_row:i_row + 1, :]

    axis_names = ['x', 'y', 'z']
    x_y_zs = []

    if coord_type == 'local':
        coord_str = 'pose'
    elif coord_type == 'global':
        coord_str = 'pose_world'
    else:
        print('Warning! Bad coord_type!')

    for axis_name in axis_names:
        columns_i = [f"{coord_str}__{point}_{axis_name}" for point in points]
        x_y_zs += [df_pose[columns_i]]

    if to_np:
        for i in range(len(x_y_zs)):
            x_y_zs[i] = x_y_zs[i].to_numpy()
            if x_y_zs[i].shape[0] == 1:
                x_y_zs[i] = x_y_zs[i].reshape(-1)

    xs, yx, zs = x_y_zs

    return xs, yx, zs


def get_main_bodypart_tensor(pose_tensor, points_axis=2):
    points = list(range(0, 23))
    return pose_tensor.take(points, axis=points_axis)


def get_main_bodypart_coordinates(df_pose, i_row=0, to_np=True, coord_type='local'):
    points = list(range(0, 23))
    return get_pose_points_coordinates(df_pose, points, i_row, to_np=to_np, coord_type=coord_type)


def get_allbody_coordinates(df_pose, i_row=0, to_np=True, coord_type='local'):
    points = list(range(0, 33))
    return get_pose_points_coordinates(df_pose, points, i_row, to_np=to_np, coord_type=coord_type)


def get_hands_line_coordinates(df_pose, i_row=0, to_np=True, coord_type='local'):
    points = [21, 15, 17, 19, 15, 13, 11, 12, 14, 16, 20, 18, 16, 22]
    return get_pose_points_coordinates(df_pose, points, i_row, to_np=to_np, coord_type=coord_type)


def get_face_line_coordinates(df_pose, i_row=0, to_np=True, coord_type='local'):
    points = [0, 10, 9, 0, 8, 6, 5, 4, 0, 7, 3, 2, 1, 0]
    return get_pose_points_coordinates(df_pose, points, i_row, to_np=to_np, coord_type=coord_type)


def get_boxbody_line_coordinates(df_pose, i_row=0, to_np=True, coord_type='local'):
    points = [11, 12, 24, 23, 11]
    return get_pose_points_coordinates(df_pose, points, i_row, to_np=to_np, coord_type=coord_type)


def get_legs_line_coordinates(df_pose, i_row=0, to_np=True, coord_type='local'):
    points = [28, 32, 30, 28, 26, 24, 23, 25, 27, 29, 31, 27]
    return get_pose_points_coordinates(df_pose, points, i_row, to_np=to_np, coord_type=coord_type)


"""
Tensors: np.array[xs, yx, zs]
"""


def get_pose_points_tensor(df_pose, points, i_row='all', xyz_axis=0, coord_type='local'):
    xs, yx, zs = get_pose_points_coordinates(df_pose, points, i_row=i_row, coord_type=coord_type, to_np=True)
    tensor = np.stack([xs, yx, zs], axis=xyz_axis)
    return tensor


def get_pose_tensor(df_pose, i_row='all', xyz_axis=0, coord_type='local'):
    points = list(range(0, 33))
    return get_pose_points_tensor(df_pose, points, i_row, xyz_axis=xyz_axis, coord_type=coord_type)


# 3) transform coordinates (scale, rotate)

"""
    Transform local coordinates in poxels into global coordinates in meters using mediapipe global coordinate
"""

def loc_to_glob_predict(loc_t, glob_t, n=-1):
    if n == -1:
        n == glob_t.shape[-1]

    glob_t_predicted = glob_t.copy()

    for i in range(glob_t_predicted.shape[1]):
        X1 = loc_t[:, i, 0:n].T
        Y1 = glob_t[:, i, 0:n]

        lr_list = [LinearRegression() for i in range(Y1.shape[0])]

        for ii, lr in enumerate(lr_list):
            lr.fit(X1, Y1[ii])

        for j in range(glob_t_predicted.shape[0]):
            glob_t_predicted[j, i, :] = lr_list[j].predict(loc_t[:, i, :].T)

    return glob_t_predicted


def load_sample(load_path, sep_in='.', sep_out='__', sep_coord='_'):
    df = pd.read_csv(load_path)
    # print(list(df_row.columns))

    new_columns = []
    for column in df.columns:

        column_splitted = column.split(sep_in)
        # print(column_splitted)
        if column_splitted[0] == 'pose':
            enum = POSE_ENUM
            # print(enum[column_splitted[1].upper()])
            column_splitted[1] = str(enum[column_splitted[1].upper()])
            column = column_splitted[0] + sep_out + column_splitted[1] + sep_coord + column_splitted[2]
        elif column_splitted[0] == 'world_pose':
            enum = POSE_ENUM
            # print(enum[column_splitted[1].upper()])
            column_splitted[1] = str(enum[column_splitted[1].upper()])
            column = 'pose_world' + sep_out + column_splitted[1] + sep_coord + column_splitted[2]

        new_columns += [column]

    df.columns = new_columns
    return df


class CoordinateTransformer:
    def __init__(self, normalize=True, n=-1):
        self.lr_list = None
        self.n = n
        self.normalize = normalize

    def fit(self, loc_t, glob_t, n_train=-1):
        if self.n == -1:
            self.n == glob_t.shape[-1]

        if n_train == -1:
            n_train = glob_t.shape[1]

        self.lr_list = [LinearRegression(normalize=self.normalize) for i in range(glob_t.shape[0])]
        # X1_list, Y1_list = [], []
        # for i in range(n_train):
        i = n_train
        X1 = loc_t[:, i, 0:self.n].T
        Y1 = glob_t[:, i, 0:self.n]
        # X1_list += [X1]
        # Y1_list += [Y1]

        # X1 = np.concatenate(X1_list, axis=0)
        # Y1 = np.concatenate(Y1_list, axis=1)

        for i, lr in enumerate(self.lr_list):
            lr.fit(X1, Y1[i])

        pass

    def predict(self, loc_t):
        glob_t_predicted = loc_t.copy()

        for i in range(glob_t_predicted.shape[1]):
            for j in range(glob_t_predicted.shape[0]):
                glob_t_predicted[j, i, :] = self.lr_list[j].predict(loc_t[:, i, :].T)

        return glob_t_predicted
