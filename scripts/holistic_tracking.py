import cv2
import mediapipe as mp
import time
from modules import auxiliary_functions as aux_funs


class HolisticDetector:
    model_type = 'holistic'
    def __init__(self, static_image_mode=False,
                 model_complexity=1,
                 smooth_landmarks=True,
                 enable_segmentation=True,
                 smooth_segmentation=True,
                 refine_face_landmarks=False,
                 min_detection_confidence=0.5,
                 min_tracking_confidence=0.5):

        self.static_image_mode = static_image_mode
        self.model_complexity = model_complexity
        self.smooth_landmarks = smooth_landmarks
        self.enable_segmentation = enable_segmentation
        self.smooth_segmentation = smooth_segmentation
        self.refine_face_landmarks = refine_face_landmarks
        self.min_detection_confidence = min_detection_confidence
        self.min_tracking_confidence = min_tracking_confidence

        self.mp_holistic = mp.solutions.holistic
        self.holistic = self.mp_holistic.Holistic(
            static_image_mode=self.static_image_mode,
            model_complexity=self.model_complexity,
            smooth_landmarks=self.smooth_landmarks,
            enable_segmentation=self.enable_segmentation,
            smooth_segmentation=self.smooth_segmentation,
            refine_face_landmarks=self.refine_face_landmarks,
            min_detection_confidence=self.min_detection_confidence,
            min_tracking_confidence=self.min_tracking_confidence)

        self.results = None


        # additional options: drawing
        self.mp_drawing = mp.solutions.drawing_utils

    def recognize(self, img, color_convert=None, draw_mode=1, ):
        '''
        Rocognize the targets objects on the image
        :param img: image to recognize
        :param color_convert: based on cv2, used in cv2.cvtColor(img, color_convert),
         the image should convert to RGB color representation (e.g. color_convert= cv2.COLOR_BGR2RGB)
        :param draw_mode: 0 or False - no drawing, 1 - drawing both hands and pose
        :return: results. Use results.<landmark name> to get landmarks. (e.g. results.pose_landmarks,
         results.left_hand_landmarks)
        '''

        if color_convert is not None:
            imgRGB = cv2.cvtColor(img, color_convert)
        else:
            imgRGB = img
        self.results = self.holistic.process(imgRGB)

        results = self.results
        mp_drawing = self.mp_drawing
        mp_holistic = self.mp_holistic

        if draw_mode:
            # Pose
            if (draw_mode == 1) or (draw_mode is True):
                if results.pose_landmarks:
                    mp_drawing.draw_landmarks(
                        img,
                        results.pose_landmarks,
                        mp_holistic.POSE_CONNECTIONS,
                        # landmark_drawing_spec=mp_drawing_styles
                        #     .get_default_pose_landmarks_style()
                    )
                if results.left_hand_landmarks:
                    mp_drawing.draw_landmarks(
                        img,
                        results.left_hand_landmarks,
                        mp_holistic.HAND_CONNECTIONS,
                        landmark_drawing_spec=mp_drawing.DrawingSpec(color=(255, 0, 0), thickness=2,
                                                                     circle_radius=3),
                        connection_drawing_spec=mp_drawing.DrawingSpec(color=(0, 255, 0), thickness=2),
                        # landmark_drawing_spec=mp_drawing_styles
                        #     .get_default_pose_landmarks_style()
                    )
                if results.right_hand_landmarks:
                    mp_drawing.draw_landmarks(
                        img,
                        results.right_hand_landmarks,
                        mp_holistic.HAND_CONNECTIONS,
                        landmark_drawing_spec=mp_drawing.DrawingSpec(color=(0, 0, 255), thickness=2,
                                                                     circle_radius=3),
                        connection_drawing_spec=mp_drawing.DrawingSpec(color=(0, 255, 0), thickness=2),
                        # landmark_drawing_spec=mp_drawing_styles
                        #     .get_default_pose_landmarks_style()
                    )
        return results


    def get_row_data(self):
        '''
        Get data in row (list)
        :return: results data in row format (list)
        '''
        results_in_dict = self.get_data_in_dict()

        row_data = aux_funs.get_row(results_in_dict, self.model_type)
        return row_data

    def get_data_in_dict(self):
        '''
        Get data in dict (dict)
        :return: results data in dict format (dict)
        '''
        results = self.results
        if results is None:
            results_left_hand, results_right_hand, results_pose = None, None, None
        else:
            results_left_hand, results_right_hand = results.left_hand_landmarks, results.right_hand_landmarks
            results_pose, results_pose_world  = results.pose_landmarks, results.pose_world_landmarks

        results_in_dict = {'right_hand': results_right_hand, 'left_hand': results_left_hand, 'pose': results_pose,
                           'pose_world': results_pose_world,
                           }

        return results_in_dict







def main():
    cap = cv2.VideoCapture(0)
    p_time, c_time = 0, 0

    holistic_detector = HolisticDetector()

    # Check if camera opened successfully
    if (cap.isOpened() == False):
        print("Error opening video stream or file")
    pass

    while True:
        success, img = cap.read()



        results = holistic_detector.recognize(img, cv2.COLOR_BGR2RGB, True)
        print(holistic_detector.get_row_data())





        c_time = time.time()
        fps = 1 / (c_time - p_time)
        p_time = c_time
        img = cv2.flip(img, 1)
        cv2.putText(img, str(int(fps)), (10, 70), cv2.FONT_HERSHEY_PLAIN, 3, (255, 0, 255), 3)

        cv2.imshow("Image", img)
        k = cv2.waitKey(1)
        if k == 27:  # close on ESC key
            cv2.destroyAllWindows()
            break


if __name__ == '__main__':
    main()
