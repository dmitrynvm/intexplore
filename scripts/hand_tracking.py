import cv2
import mediapipe as mp
from Gesture_recognition.modules import auxiliary_functions as aux_funs
import time


class HandDetector:
    model_type = 'hand'

    def __init__(self, static_image_mode=False,
                 max_num_hands=2,
                 model_complexity=1,
                 min_detection_confidence=0.5,
                 min_tracking_confidence=0.5):
        self.static_image_mode = static_image_mode
        self.max_num_hands = max_num_hands
        self.model_complexity = model_complexity
        self.min_detection_confidence = min_detection_confidence
        self.min_tracking_confidence = min_tracking_confidence

        self.mp_hands = mp.solutions.hands
        self.hands = self.mp_hands.Hands(self.static_image_mode, self.max_num_hands, self.model_complexity,
                                         self.min_detection_confidence, self.min_tracking_confidence)

        self.results = None
        self.hands_num = 0

        # additional options: drawing
        self.mp_drawing = mp.solutions.drawing_utils




    def recognize(self, img, color_convert=None, draw=True, ):

        if color_convert is not None:
            imgRGB = cv2.cvtColor(img, color_convert)
        else:
            imgRGB = img
        self.results = self.hands.process(imgRGB)

        results = self.results
        mp_drawing = self.mp_drawing
        mp_hands = self.mp_hands

        if results.multi_hand_landmarks:
            self.hands_num = len(results.multi_hand_landmarks)
        else:
            self.hands_num = 0

        if draw:
            if results.multi_hand_landmarks:  # if not None
                for handLms in results.multi_hand_landmarks:
                    mp_drawing.draw_landmarks(img, handLms, mp_hands.HAND_CONNECTIONS,
                                          # landmark_drawing_spec=mpDraw.DrawingSpec(color=(255, 0, 255), thickness=3,
                                          #                                          circle_radius=5),
                                          connection_drawing_spec=mp_drawing.DrawingSpec(color=(0, 255, 0),
                                                                                         thickness=2))
        return results

    def find_hand_position(self, img, hand_id=0, mark_points=[]):
        '''
        Return lm_list of hand by hand_id and draw points in mark_points list
        :param img:
        :param hand_id:
        :param mark_points:
        :return:
        '''
        lm_list = []
        if isinstance(mark_points, int):
            mark_points = [mark_points]

        if self.results.multi_hand_landmarks:  # if not None
            hand = self.results.multi_hand_landmarks[hand_id]
            for id, lm in enumerate(hand.landmark):
                h, w, c = img.shape
                cx, cy = int(lm.x * w), int(lm.y * h)
                lm_list.append([id, cx, cy, lm.z])

                if id in mark_points:
                    cv2.circle(img, (cx, cy), 15, (255, 0, 255), cv2.FILLED)

        return lm_list

    def get_hands_number(self):
        return self.hands_num


    def get_row_data(self, hand_id=0):
        '''
        Get data in row (list)
        :return: results data in row format (list)
        '''
        results_in_dict = self.get_data_in_dict(hand_id)
        row_data = aux_funs.get_row(results_in_dict, self.model_type)
        return row_data

    def get_data_in_dict(self, hand_id=0):
        # !!!!! sync with hand id!!!
        '''
        Get data in dict
        :return: results data in dict format
        '''
        results = self.results

        if results is None:
            results_hand = None
        else:
            results_hand = results.multi_hand_landmarks[hand_id]

        results_in_dict = {'hand': results_hand}
        return results_in_dict




def main():
    cap = cv2.VideoCapture(0)
    p_time, c_time = 0, 0

    handDetector = HandDetector()

    # Check if camera opened successfully
    if (cap.isOpened() == False):
        print("Error opening video stream or file")
    pass

    while True:
        success, img = cap.read()


        results = handDetector.recognize(img, cv2.COLOR_BGR2RGB, True)
        if results.multi_hand_landmarks:
            for hand_id in range(len(results.multi_hand_landmarks)):
                lm_list = handDetector.find_hand_position(img, hand_id, [0, 4, 12, 8, 16, 20])
                # print(lm_list[4])
                print(handDetector.get_row_data(hand_id))

        c_time = time.time()
        fps = 1 / (c_time - p_time)
        p_time = c_time
        img = cv2.flip(img, 1)
        cv2.putText(img, str(int(fps)), (10, 70), cv2.FONT_HERSHEY_PLAIN, 3, (255, 0, 255), 3)

        cv2.imshow("Image", img)
        k = cv2.waitKey(1)
        if k == 27:  # close on ESC key
            cv2.destroyAllWindows()
            break


if __name__ == '__main__':
    main()
