import cv2
import time
import sys
import os

sys.path.append('..')

from holistic_tracking import HolisticDetector
from modules.auxiliary_functions import Saver



""" From webcam"""
INPUT_SOURCE = 0
FROM_VIDEO_RECORD = False


""" From video"""
# source_record_name = 'rgb.mp4'
# INPUT_SOURCE = f'.\\Data\\{source_record_name}'
# FROM_VIDEO_RECORD = True



""" Saving parameters"""
SAVE_CSV, SAVE_PKL, SAVE_SETTINGS, SAVE_VIDEO = [False]*4

# SAVE_CSV, SAVE_PKL, SAVE_SETTINGS = True, True, True
# SAVE_VIDEO = True


cap = cv2.VideoCapture(INPUT_SOURCE)
p_time, c_time, t_start = 0, 0, time.time()
n_save = 100


holistic_detector = HolisticDetector()
saver = Saver()

if SAVE_VIDEO:
    output = saver.start_recording(cap, fps=30)

saver.start_saving(SAVE_CSV, SAVE_PKL, SAVE_SETTINGS)


# Check if camera opened successfully
if (cap.isOpened() == False):
    print("Error opening video stream or file")
pass

n_frame = 0
event = -1
while cap.isOpened():
    success, img = cap.read()

    if not success:
        print("Ignoring empty camera frame.")
        # If loading a video, use 'break' instead of 'continue'
        if FROM_VIDEO_RECORD:
            break
        else:
            continue
    n_frame += 1

    results = holistic_detector.recognize(img, cv2.COLOR_BGR2RGB, True)

    c_time = time.time()
    timestamp = c_time - t_start

    #saving
    saver.add_data(timestamp, [holistic_detector.get_row_data(), holistic_detector.get_data_in_dict()], event,
                   save_period=n_save, frame_number=n_frame)

    fps = 1 / (c_time - p_time)
    p_time = c_time

    # Rotate and flip!!!
    img = cv2.flip(img, 1)
    if FROM_VIDEO_RECORD:
        img = cv2.rotate(img, cv2.ROTATE_180)

    height, width = img.shape[:2]
    text = f"N:{n_frame} fps:{int(fps)} {width}x{height}"
    cv2.putText(img, text, (10, 30), cv2.FONT_HERSHEY_PLAIN, 2, (255, 0, 255), 1)

    if SAVE_VIDEO:
        saver.record_frame(img)

    cv2.imshow("Image", img)
    k = cv2.waitKey(1)
    print(n_frame, k)
    event = k
    if k == 27:  # close on ESC key
        cv2.destroyAllWindows()
        break



cap.release()
saver.stop_recording()
cv2.destroyAllWindows()
saver.save()
