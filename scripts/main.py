import cv2
import mediapipe as mp
import time
import os
import numpy as np
import pandas as pd

import pickle



# class HandDetector:
#     def __init__(self, static_image_mode=False,
#                max_num_hands=2,
#                model_complexity=1,
#                min_detection_confidence=0.5,
#                min_tracking_confidence=0.5):
#         self.static_image_mode = static_image_mode
#         self.max_num_hands = max_num_hands
#         self.model_complexity = model_complexity
#         self.min_detection_confidence = min_detection_confidence
#         self.min_tracking_confidence = min_tracking_confidence
#
#         self.mpHands = mp.solutions.hands
#         self.hands = self.mpHands.Hands(self.static_image_mode,  self.max_num_hands, self.model_complexity,
#                               self.min_detection_confidence, self.min_tracking_confidence)
#         self.mpDraw = mp.solutions.drawing_utils
#
#     def find_hands(self, img,  color_convert=None, draw=True,):
#
#         if color_convert is not None:
#             imgRGB = cv2.cvtColor(img, color_convert)
#         else:
#             imgRGB = img
#         self.results = self.hands.process(imgRGB)
#
#         results = self.results
#         mpDraw = self.mpDraw
#         mpHands = self.mpHands
#
#         if draw:
#             if results.multi_hand_landmarks:  # if not None
#                 for handLms in results.multi_hand_landmarks:
#
#                     mpDraw.draw_landmarks(img, handLms, mpHands.HAND_CONNECTIONS,
#                                           # landmark_drawing_spec=mpDraw.DrawingSpec(color=(255, 0, 255), thickness=3,
#                                           #                                          circle_radius=5),
#                                           connection_drawing_spec=mpDraw.DrawingSpec(color=(0, 255, 0), thickness=2))
#         return results
#
#
#     def find_hand_position(self, img, hand_id=0, mark_points=[]):
#         lm_list = []
#         if isinstance(mark_points, int):
#             mark_points = [mark_points]
#
#         if self.results.multi_hand_landmarks:  # if not None
#             hand = self.results.multi_hand_landmarks[hand_id]
#             for id, lm in enumerate(hand.landmark):
#                 h, w, c = img.shape
#                 cx, cy = int(lm.x * w), int(lm.y * h)
#                 # print(id, cx, cy, lm.z)
#                 lm_list.append([id, cx, cy, lm.z])
#
#                 if id in mark_points:
#                     cv2.circle(img, (cx, cy), 7, (255, 0, 255), cv2.FILLED)
#
#         return lm_list


mp_holistic = mp.solutions.holistic
# mp_pose = mp.solutions.pose
mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles


# pose = mp_pose.Pose(
#     static_image_mode=False,
#     model_complexity=0,
#     smooth_landmarks=True,
#     enable_segmentation=True,
#     smooth_segmentation=False,
#     min_detection_confidence=0.7,
#     min_tracking_confidence=0.7
# )

# cap = cv2.VideoCapture(0)

# cap = cv2.VideoCapture(f'Data//{gesture_name}.wmv')
cap = cv2.VideoCapture(f'Data//rgb.mp4')
p_time, c_time, t_start = 0, 0, time.time()
n_save = 100
save_counter = 0

SAVE_VIDEO = True

#
#
#
#
# save_name_base = 'record_'
# record_inx = 1
# save_name = save_name_base + str(record_inx)
# save_path_csv = f'Data\\{save_name}.csv'
# while os.path.exists(save_path_csv):
#     record_inx +=1
#     save_name = save_name_base + str(record_inx)
#     save_path_csv = f'Data\\{save_name}.csv'
# print(record_inx, save_path_csv)
# save_path_pkl = save_path_csv[:-3] + 'pkl'
save_path_csv = 'Data\\temp.'
if SAVE_VIDEO:
    # Obtain frame size information using get() method
    frame_width = int(cap.get(3))
    frame_height = int(cap.get(4))
    frame_size = (frame_width, frame_height)
    fps = 30

    # Initialize video writer object
    output = cv2.VideoWriter(save_path_csv[:-3] + 'avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 20, frame_size)

print(int(output.get(3)))




#
#


max_num_list = [33, 21, 21]
col_names = [f"{model}_{num}_{metric}" for model, max_num in zip(['pose', 'left', 'right'], max_num_list)\
             for num in range(max_num+1) for metric in ['x', 'y', 'z']]
print(col_names)
data = pd.DataFrame(columns=col_names)
results_list= []



data.to_csv(save_path_csv, index=False)
with open(save_path_pkl, 'wb') as handle:
    pickle.dump(results_list, handle)




holistic = mp_holistic.Holistic(
               static_image_mode=False,
               model_complexity=1,
               smooth_landmarks=True,
               enable_segmentation=True,
               smooth_segmentation=True,
               refine_face_landmarks=False,
               min_detection_confidence=0.5,
               min_tracking_confidence=0.5
)


# handDetector = HandDetector(model_complexity=1, max_num_hands=2)

# Check if camera opened successfully
if (cap.isOpened() == False):
    print("Error opening video stream or file")
pass

while cap.isOpened():
    success, img = cap.read()

    if not success:
        print("Ignoring empty camera frame.")
        # If loading a video, use 'break' instead of 'continue'.
        continue

    results = holistic.process(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))

    



    # results = pose.process(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))


    # mp_drawing.draw_landmarks(
    #     img,
    #     results.face_landmarks,
    #     mp_holistic.FACEMESH_CONTOURS,
    #     # landmark_drawing_spec=None,
    #     # connection_drawing_spec=mp_drawing_styles
    #     #     .get_default_face_mesh_contours_style()
    #     )
    mp_drawing.draw_landmarks(
        img,
        results.pose_landmarks,
        mp_holistic.POSE_CONNECTIONS,
        # landmark_drawing_spec=mp_drawing_styles
        #     .get_default_pose_landmarks_style()
    )
    # condition = np.stack((results.segmentation_mask,)*3, axis=-1)>0.7
    # # mask = mask.astype( np.bool)
    #
    # bg_image = np.zeros(img.shape, dtype=np.uint8)
    #
    # img = np.where(condition, img, bg_image)
    # if bg_image is None:
    #     bg_image = np.zeros(img.shape, dtype=np.uint8)
    #     bg_image[:] = BG_COLOR
    # output_image = np.where(condition, image, bg_image)
    # np.where(condition, image, bg_image)
    # cv2.bitwise_and(img, img, mask=mask)


    mp_drawing.draw_landmarks(
        img,
        results.left_hand_landmarks,
        mp_holistic.HAND_CONNECTIONS,
        landmark_drawing_spec=mp_drawing.DrawingSpec(color=(255, 0, 0), thickness=2, circle_radius=3),
        connection_drawing_spec=mp_drawing.DrawingSpec(color=(0,255, 0), thickness=2),
        # landmark_drawing_spec=mp_drawing_styles
        #     .get_default_pose_landmarks_style()
    )

    mp_drawing.draw_landmarks(
        img,
        results.right_hand_landmarks,
        mp_holistic.HAND_CONNECTIONS,
        landmark_drawing_spec=mp_drawing.DrawingSpec(color=(0, 0, 255), thickness=2, circle_radius=3),
        connection_drawing_spec=mp_drawing.DrawingSpec(color=(0, 255, 0), thickness=2),
        # landmark_drawing_spec=mp_drawing_styles
        #     .get_default_pose_landmarks_style()
    )

    # results_hand = handDetector.find_hands(img, cv2.COLOR_BGR2RGB, True)
    # if results_hand.multi_hand_landmarks:
    #     for hand_id in range(len(results_hand.multi_hand_landmarks)):
    #         lm_list = handDetector.find_hand_position(img, hand_id, [0, 4, 12, 8, 16, 20])
    #         print(lm_list[4])

    c_time = time.time()

    #saving
    t = c_time - t_start
    if results is None:
        results_left = None
        results_right = None
        results_pose = None
    else:
        results_left = results.left_hand_landmarks
        results_right = results.right_hand_landmarks
        results_pose = results.pose_landmarks




    results_list.append({'t': t,'right': results_right, 'left': results_left, 'pose':  results_pose})

    if save_counter > n_save:
        save_counter = 0
        # data.to_csv(save_path_csv, index=False)
        with open(save_path_pkl, 'wb') as handle:
            pickle.dump(results_list, handle)
        print("Saving...")

    else:
        save_counter += 1




    fps = 1 / (c_time - p_time)
    p_time = c_time
    img = cv2.flip(img, 1)
    img = cv2.rotate(img, cv2.ROTATE_180)
    cv2.putText(img, str(int(fps)), (10, 70), cv2.FONT_HERSHEY_PLAIN, 3, (255, 0, 255), 3)

    if SAVE_VIDEO:
        output.write(img)
    cv2.imshow("Image", img)
    k = cv2.waitKey(1)
    if k == 27:  # close on ESC key
        cv2.destroyAllWindows()
        break




cap.release()
output.release()


# Closes all the frames
cv2.destroyAllWindows()

# data.to_csv(save_path_csv, index=False)
with open(save_path_pkl, 'wb') as handle:
    pickle.dump(results_list, handle)
print("Saving...")


# def main():
#     cap = cv2.VideoCapture(0)
#     p_time, c_time = 0, 0
#
#     handDetector = HandDetector()
#
#     # Check if camera opened successfully
#     if (cap.isOpened() == False):
#         print("Error opening video stream or file")
#     pass
#
#     while True:
#         success, img = cap.read()
#
#         # imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
#
#
#         results = handDetector.find_hands(img, cv2.COLOR_BGR2RGB, True)
#         if results.multi_hand_landmarks:
#             for hand_id in range(len(results.multi_hand_landmarks)):
#                 lm_list = handDetector.find_hand_position(img, hand_id, [0, 4, 12, 8, 16, 20])
#                 print(lm_list[4])
#
#         c_time = time.time()
#         fps = 1 / (c_time - p_time)
#         p_time = c_time
#         img = cv2.flip(img, 1)
#         cv2.putText(img, str(int(fps)), (10, 70), cv2.FONT_HERSHEY_PLAIN, 3, (255, 0, 255), 3)
#
#         cv2.imshow("Image", img)
#         k = cv2.waitKey(1)
#         if k == 27:  # close on ESC key
#             cv2.destroyAllWindows()
#             break
#
#
# if __name__ == '__main__':
#     main()