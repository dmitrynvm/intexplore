import cv2
from hand_tracking import HandDetector
from holistic_tracking import HolisticDetector
import mediapipe as mp
import time

cap = cv2.VideoCapture(0)


img = cv2.imread('../Data/color0144.png')

cv2.imshow("Image", img)
cv2.waitKey(500)

# y, x = 400, 800
# h, w, = 500, 400
# img = img[y:y+h, x:x+w]
handDetector = HandDetector(static_image_mode=True, min_detection_confidence=0.1,
               min_tracking_confidence=0.1)
holistic = HolisticDetector()

results = holistic.recognize(img)
print(len(results.pose_world_landmarks))

#

# results = handDetector.find_hands(img)
# print(results.multi_hand_landmarks is not None)
# if results.multi_hand_landmarks:
#     for hand_id in range(len(results.multi_hand_landmarks)):
#         lm_list = handDetector.find_hand_position(img, hand_id,)
#         print(lm_list)

cv2.imshow("Image", img)
cv2.waitKey(0)


