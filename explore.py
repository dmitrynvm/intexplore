import os
import cv2
import mediapipe as mp
import numpy as np
import streamlit as st
import plotly.graph_objects as go
from fnmatch import fnmatch
from modules.preproc import *
from PIL import Image

st.set_page_config(layout="wide")
mp_drawing = mp.solutions.drawing_utils
mp_hands = mp.solutions.hands
mph = mp.solutions.holistic
count = 0
last = 0


def walk(path, regex):
    outputs = []
    match = lambda line, ptns: max([fnmatch(line, ptn) for ptn in ptns])
    for _, _, file_names in os.walk(path):
        for file_name in file_names:
            if match(file_name, regex):
                outputs.append(file_name)
    return outputs


def get_files(path, regex):
    files = []
    file_names = walk(path, regex)
    for file_name in sorted(file_names):
        file_path = os.path.join(path, file_name)
        files.append(file_path)
    return files


files = get_files('data/export_left1', '*.jpg')


def load_frame(n: int):
    data_dir = 'data'
    file_name = 'keypoints.csv'
    data_path = 'export_left1'
    load_path = os.path.join(data_dir, data_path, file_name)
    df = load_sample(load_path, sep_in='__', sep_out='__', sep_coord='_')
    tensor = get_pose_tensor(df, coord_type='global')
    tensor[:, :, :] = tensor[[0, 2, 1], :, :]
    tensor[2] *= -1
    tensor[2] = tensor[2] - tensor[2].min(axis=1, keepdims=True)

    xyzs = tensor[:, :, POSE_POINTS]
    xyzs_h = tensor[:, :, HANDS_LINE_POINTS]
    xyzs_f = tensor[:, :, FACE_LINE_POINTS]
    xyzs_b = tensor[:, :, BOXBODY_LINE_POINTS]
    xyzs_l = tensor[:, :, LEGS_LINE_POINTS]

    frames = []
    for i in range(0, 121, 1):
        xyz = xyzs
        xs, ys, zs = xyz[:, n, :]
        data_i = [
            go.Scatter3d(
                x=xs,
                y=ys,
                z=zs,
                mode='markers',
                marker=dict(
                    size=2,
                    color='coral',
                    colorscale='Viridis'
                )
            )
        ]
        points_list = [
            xyzs_h[:, n, :],
            xyzs_f[:, n, :],
            xyzs_l[:, n, :],
            xyzs_b[:, n, :]
        ]
        data_lines = [
            go.Scatter3d(
                x=points[0],
                y=points[1],
                z=points[2],
                mode='lines'
            ) for points in points_list
        ]
        data_i += data_lines
        frames += [
            go.Frame(
                data=data_i,
                name=str(n)
            )
        ]
    return frames


def draw_body(n: int):
    frame = load_frame(n)

    fig = go.Figure(
        data=frame[0]['data'],
        layout=go.Layout(
            scene=dict(
                xaxis=dict(
                    nticks=5,
                    range=[-1, 1],
                    autorange=False,
                    visible=False,
                ),
                yaxis=dict(
                    nticks=14,
                    range=[-1, 1],
                    autorange=False,
                    visible=False
                ),
                zaxis=dict(
                    nticks=6,
                    range=[0, 2],
                    autorange=False,
                    visible=False
                ),
                aspectmode='cube',
            ),
            width=700,
            height=500,
            xaxis=dict(range=[0, 5], autorange=False),
            yaxis=dict(range=[0, 5], autorange=False),
            hovermode="closest",
            showlegend=False
        ),
        frames=frame,
        layout_showlegend=False
    )
    camera = dict(
        up=dict(x=0, y=0, z=0.1),
        center=dict(x=0, y=0, z=0),
    )
    fig.update_layout(scene_camera=camera)
    return fig


def draw_image(image):
    holistic = mph.Holistic(
        static_image_mode=True,
        model_complexity=2,
        smooth_landmarks=True,
        enable_segmentation=True,
        smooth_segmentation=True,
        refine_face_landmarks=True
    )
    with mp_hands.Hands(
        static_image_mode=True,
    ) as hands:
        image = cv2.flip(image, 1)
        results = holistic.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
        annotated_image = image.copy()
        mp_drawing.draw_landmarks(
            annotated_image, results.pose_landmarks, mph.POSE_CONNECTIONS)

    return annotated_image


def draw_layout(n):
    global count
    # st.title('Explore')
    gesture = st.selectbox('Gesture', ('close_left1',), on_change=None, key=f'gesture{count}')
    frame = st.slider('Frames', min_value=0, max_value=len(files)-2, value = last, on_change=move, key=f'frame{count}')
    col1, col2 = st.columns((1, 1))
    # col1.subheader('Skeleton')
    # col2.subheader('Image')
    figure = draw_body(n)
    col1.plotly_chart(figure, config={'displayModeBar': False})
    uploaded = cv2.imread(files[n])
    annotated = draw_image(uploaded)
    annotated = cv2.cvtColor(annotated, cv2.COLOR_BGR2RGB)
    col2.image(cv2.flip(annotated, 1))
    count += 1


def move():
    global last
    last = st.session_state[f'frame{count-1}']
    draw_layout(st.session_state[f'frame{count-1}'])


def main():
    draw_layout(0)



if __name__ == "__main__":
    main()